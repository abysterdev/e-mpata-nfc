package com.example.empatanfc;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import com.example.e_mpatanfc.RestClients.TransactionClient;
import com.example.e_mpatanfc.models.models.Transaction;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;

public class Login extends Activity {
    JSONObject params = new JSONObject();
    JSONObject params2 = new JSONObject();
    RequestParams param3= new RequestParams();
    private String token;
    private String token3;
    private boolean isLoggedIn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        SharedPreferences sharedPref= PreferenceManager.getDefaultSharedPreferences(Login.this);
        isLoggedIn= sharedPref.getBoolean("isLoggedIn",false);
        if (isLoggedIn) {
            Intent i = new Intent(Login.this, home.class);
            startActivity(i);
            finish();
        }
        ProgressBar loader= findViewById(R.id.progressBar2);
        Button button=findViewById(R. id. button3);
        EditText email=findViewById(R. id. editTextTextEmailAddress);
        EditText pass=findViewById(R. id. editTextTextPassword);
        ScrollView scroll=findViewById(R.id.scroll2);
        scroll.setVerticalScrollBarEnabled(false);
        scroll.setHorizontalScrollBarEnabled(false);
        pass.setOnKeyListener(new View.OnKeyListener(){
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
                    if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                            connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
                        try {
                            loader.setVisibility(View.VISIBLE);
                            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                            authenticate();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }
                    }else{
                        Toast.makeText(getApplicationContext(),"Veuillez vous connecter à Internet !",Toast.LENGTH_LONG).show();
                    }
                }
                return false;
            }
        });
        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                ConnectivityManager connectivityManager = (ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
                if(connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE).getState() == NetworkInfo.State.CONNECTED ||
                        connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).getState() == NetworkInfo.State.CONNECTED) {
                try {
                    authenticate();
                    loader.setVisibility(View.VISIBLE);
                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                            WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                }else{
                    Toast.makeText(getApplicationContext(),"Veuillez vous connecter à Internet !",Toast.LENGTH_LONG).show();
                }

            }
        });
    }
    private void authenticate() throws JSONException, UnsupportedEncodingException {
        params2.put("application","xXMXlK5dsy2mCHY9Li0Q");
        params2.put("password","password");
        params2.put("username","admin@wallet.com");
        StringEntity entity = new StringEntity(params2.toString());
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(Login.this, "http://51.38.42.38:8080/ws/authenticate", entity,"application/json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response){
                try {
                    login(response.optString("jwttoken"));
                } catch (JSONException | UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                token=response.optString("jwttoken");
                Log.v("token",token);

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                token3=errorResponse.toString();
                Log.v("token",token3);
            }


        });

    }

    private void login(String token) throws JSONException, UnsupportedEncodingException {
        List<Header> headers2 = new ArrayList<Header>();
        headers2.add(new BasicHeader("Accept", "application/json"));
        //headers.add(new BasicHeader("Content-Type", "application/json"));
        Log.v("token2",token);
        headers2.add(new BasicHeader("Authorization", "Bearer "+token));
        EditText email=findViewById(R. id. editTextTextEmailAddress);
        EditText pass=findViewById(R. id. editTextTextPassword);

        Log.v("password:",pass.getText().toString());
        params.put("email", email.getText());
        params.put("password", pass.getText());
        StringEntity entity = new StringEntity(params.toString());

        TransactionClient.get(Login.this, "user/login",headers2.toArray(new Header[headers2.size()]),entity,"application/json",
                new JsonHttpResponseHandler() {


                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response2) {

                        ProgressBar loader= findViewById(R.id.progressBar2);
                        Log.v("response",response2.optString("accountId"));
                        SharedPreferences sharedPref= PreferenceManager.getDefaultSharedPreferences(Login.this);
                        SharedPreferences.Editor editor=sharedPref.edit();
                        param3.put("Id",response2.optString("accountId"));
                        editor.putString("accountId",response2.optString("accountId"));
                        editor.putBoolean("isLoggedIn",true);
                        editor.apply();
                        Log.v("accountId",sharedPref.getString("accountId","de"));

                       TransactionClient.get2(Login.this, "account/history/"+response2.optString("accountId"),headers2.toArray(new Header[headers2.size()]),param3,
                                new JsonHttpResponseHandler() {
                                    @Override
                                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {


                                    }

                                    @Override
                                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                                        //super.onSuccess(statusCode, headers, response);
                                        List<Transaction> txs = null;
                                        int a=0;
                                        Intent i4=new Intent(Login.this,detailsTransaction.class);
                                        try {
                                            Transaction transaction=new Transaction();
                                            Gson gson = new Gson();
                                            Intent i3=new Intent(Login.this,home.class);

                                            for(int i=0;i<response.length();i++) {
                                                JSONObject object = response.getJSONObject(i);
                                                Object operationType=object.get("operationType");
                                                Object othe=object.get("other");
                                                Object acc=object.get("account");

                                                if(operationType instanceof JSONObject && acc instanceof JSONObject ) {
                                                    if (object.getJSONObject("operationType").getInt("operationTypeId") == 21 ||
                                                            object.getJSONObject("operationType").getInt("operationTypeId") == 23 ||
                                                            object.getJSONObject("operationType").getInt("operationTypeId") == 8) {
                                                        a++;
                                                        JSONObject account = object.getJSONObject("account");
                                                        if(othe instanceof JSONObject){
                                                            JSONObject other = object.getJSONObject("other");
                                                            JSONObject user2 = other.getJSONObject("user");
                                                            transaction.setUserId2(user2.getInt("userId"));
                                                            transaction.setEmailBen(user2.getString("email"));
                                                            transaction.setNomBen(user2.getString("lastName"));
                                                            transaction.setPrenomBen(user2.getString("firstName"));
                                                            transaction.setMarchant(other.getInt("msisdn"));
                                                        }

                                                        int accountId = account.getInt("accountId");
                                                        JSONObject user = account.getJSONObject("user");

                                                        String lastName = user.getString("lastName");
                                                        int operationId = object.getInt("operationId");
                                                        Log.v("operationId", String.valueOf(object.getInt("operationId")));
                                                        transaction.setReference(object.getString("reference"));
                                                        transaction.setOperationId(object.getInt("operationId"));
                                                        transaction.setDate(object.getString("operationDate"));
                                                        transaction.setUserId1(user.getInt("userId"));

                                                        transaction.setEmailEm(user.getString("email"));
                                                        transaction.setNomEm(user.getString("lastName"));

                                                        transaction.setPrenomEm(user.getString("firstName"));
                                                        transaction.setNumCard(object.getString("card"));
                                                        transaction.setType(object.getJSONObject("operationType").getString("libelle"));
                                                        transaction.setMontant(object.getInt("amount"));
                                                        transaction.setCurrency(object.getJSONObject("devise").getString("code"));
                                                        transaction.setEmetteur(String.valueOf(account.getInt("msisdn")));


                                                        String transJson = gson.toJson(transaction);
                                                        loader.setVisibility(View.INVISIBLE);
                                                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                                        //Toast.makeText(getApplicationContext(), "Login success !", Toast.LENGTH_SHORT).show();
                                                        editor.putString("transactions" + String.valueOf(i), transJson);
                                                        //Log.v("transactions:", sharedPref.getString("transactions"+i,"trans"));
                                                        editor.apply();
                                                        Log.v("ramzi", sharedPref.getString("transactions" + String.valueOf(i), "1"));
                                                    }
                                                }

                                            }

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                        editor.putInt("transNumber", a);
                                        editor.apply();
                                        Log.v("transRam", String.valueOf(sharedPref.getInt("transNumber",1)));
                                        editor.putString("accountId",response2.optString("accountId"));
                                        editor.apply();
                                        editor.putBoolean("isLoggedIn",true);
                                        editor.apply();
                                        loader.setVisibility(View.INVISIBLE);
                                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                        //Toast.makeText(getApplicationContext(),"Login success !",Toast.LENGTH_SHORT).show();
                                        Intent i3=new Intent(Login.this,home.class);
                                        Login.this.startActivity(i3);
                                        Login.this.finish();
                                    }

                                    @Override
                                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                                        Toast.makeText(getApplicationContext(),errorResponse.optString("message"),Toast.LENGTH_LONG).show();
                                    }

                                    @Override
                                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {

                                        try {
                                            Toast.makeText(getApplicationContext(),errorResponse.getJSONObject(3).toString(),Toast.LENGTH_LONG).show();
                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }

                                    @Override
                                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                                        Log.v("error",responseString);
                                    }

                                    @Override
                                    public void onSuccess(int statusCode, Header[] headers, String responseString) {

                                        Log.v("result",responseString);
                                    }
                                });


                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        ProgressBar loader= findViewById(R.id.progressBar2);
                        try {
                            Log.v("response",response.getJSONObject(0).toString());
                            SharedPreferences sharedPref= PreferenceManager.getDefaultSharedPreferences(Login.this);
                            SharedPreferences.Editor editor=sharedPref.edit();
                            editor.putString("accountId",response.getJSONObject(0).toString());
                            editor.putBoolean("isLoggedIn",true);
                            loader.setVisibility(View.INVISIBLE);
                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                           // Toast.makeText(getApplicationContext(),"Login success !",Toast.LENGTH_SHORT).show();
                            Intent i3=new Intent(Login.this,home.class);
                            Login.this.startActivity(i3);
                            Login.this.finish();
                            editor.apply();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        ProgressBar loader= findViewById(R.id.progressBar2);
                        loader.setVisibility(View.INVISIBLE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        Toast.makeText(getApplicationContext(),errorResponse.optString("message"),Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        ProgressBar loader= findViewById(R.id.progressBar2);
                        try {
                            loader.setVisibility(View.INVISIBLE);
                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                            Toast.makeText(getApplicationContext(),errorResponse.getJSONObject(3).toString(),Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        ProgressBar loader= findViewById(R.id.progressBar2);
                        loader.setVisibility(View.INVISIBLE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        Toast.makeText(getApplicationContext(),"un problème est survenu lors du login",Toast.LENGTH_LONG).show();
                    }


                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        ProgressBar loader= findViewById(R.id.progressBar2);
                        SharedPreferences sharedPref= PreferenceManager.getDefaultSharedPreferences(Login.this);
                        SharedPreferences.Editor editor=sharedPref.edit();
                        editor.putBoolean("isLoggedIn",true);
                        Log.v("responseString",responseString);
                        loader.setVisibility(View.INVISIBLE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        //Toast.makeText(getApplicationContext(),"Login success !",Toast.LENGTH_SHORT).show();
                        Intent i3=new Intent(Login.this,home.class);
                        Login.this.startActivity(i3);
                        Login.this.finish();
                    }
                });

    }
}