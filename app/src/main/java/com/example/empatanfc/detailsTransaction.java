package com.example.empatanfc;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.example.e_mpatanfc.adapters.Operation;
import com.example.e_mpatanfc.models.models.Transaction;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.ArrayList;

public class detailsTransaction extends AppCompatActivity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_transaction);
        ListView lst=findViewById(R.id.transactionList);
        ActionBar.LayoutParams params = new ActionBar.LayoutParams(//Center the textview in the ActionBar !
                Gravity.CENTER);
        getSupportActionBar().setCustomView(getLayoutInflater().inflate(R.layout.appbar, null),params);
        Gson gson = new Gson();
        SharedPreferences sharedPref= PreferenceManager.getDefaultSharedPreferences(detailsTransaction.this);
        int transNumber=sharedPref.getInt("transNumber",0);
        Log.v("ramza", String.valueOf(transNumber));
        ArrayList<Transaction> txs=new ArrayList<Transaction>();
        Operation op;
        op=new Operation(detailsTransaction.this,txs);
        Intent a = getIntent();
        for(int i=0; i<transNumber;i++){
           String trans= sharedPref.getString("transactions"+String.valueOf(i),"trans");
              Log.v("transactions2:",  trans);
            try {
                Transaction transaction = gson.fromJson(trans, Transaction.class);
                op.add(transaction);
            }catch (IllegalStateException | JsonSyntaxException exception)
            {
                exception.getMessage();
            }

        }
        lst.setAdapter(op);
        View footerView =  ((LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer, null, false);
       View headerView =  ((LayoutInflater)this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.header, null, false);
        View head= (View) findViewById(R.id.tableLayout3);
        LayoutInflater inflater = getLayoutInflater();
        View header = inflater.inflate(R.layout.header, lst, false);
       // lst.addHeaderView(headerView);
        //lst.addFooterView(footerView);
        BottomNavigationView bottomNavigation = findViewById(R.id.bottom_navigation);
        Menu menu = bottomNavigation.getMenu();
        MenuItem item = menu.getItem(1);
        item.setChecked(true);
        bottomNavigation.setOnNavigationItemSelectedListener( new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {

                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        item.setChecked(true);
                        item.setEnabled(true);
                        Intent i3=new Intent(detailsTransaction.this,home.class);
                        detailsTransaction.this.startActivity(i3);
                        detailsTransaction.this.finish();
                        return true;
                    case R.id.navigation_transactions:
                        item.setChecked(true);
                        item.setEnabled(true);
                        Intent i=new Intent(detailsTransaction.this,detailsTransaction.class);
                        detailsTransaction.this.startActivity(i);
                        detailsTransaction.this.finish();
                        return true;
                }
                return false;
            }
        });
        lst.setOnItemClickListener(new AdapterView.OnItemClickListener(){

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Transaction trans=op.getItem(position);
                Gson gson = new Gson();
                String myJson = gson.toJson(trans);
                Intent intent =new Intent(detailsTransaction.this,detailstransaction2.class);
                intent.putExtra("transItem", myJson);
                startActivity(intent);
            }
        });
        OnSwipeTouchListener onSwipeTouchListener = new OnSwipeTouchListener(detailsTransaction.this) {
            @Override
            public void onSwipeLeft() {
                Intent i3=new Intent(detailsTransaction.this,home.class);
                detailsTransaction.this.startActivity(i3);
                detailsTransaction.this.finish();
            }
        };
    }
}