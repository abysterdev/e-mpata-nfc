package com.example.empatanfc;

import android.app.Activity;
import android.app.ActivityOptions;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import com.example.empatanfc.ActionCallback;
import com.cloudpos.card.Card;
import android.nfc.tech.MifareClassic;
import android.nfc.tech.MifareUltralight;
import android.nfc.tech.Ndef;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.DeviceException;
import com.cloudpos.OperationListener;
import com.cloudpos.OperationResult;
import com.cloudpos.POSTerminal;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import com.cloudpos.TimeConstants;
import com.cloudpos.rfcardreader.RFCardReaderDevice;
import com.cloudpos.rfcardreader.RFCardReaderOperationResult;
import com.example.e_mpatanfc.RestClients.TransactionClient;
import com.example.e_mpatanfc.models.models.Transaction;
import com.google.android.material.bottomnavigation.BottomNavigationMenu;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.textfield.TextInputLayout;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.vistrav.pop.Pop;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.sql.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;
import cz.msebera.android.httpclient.message.BasicHeader;


public class home extends AppCompatActivity implements AdapterView.OnItemSelectedListener {
    NfcAdapter nfcAdapter;
    PendingIntent pendingIntent;
    final static String TAG = "nfc_test";
    private ListView noteList;
    JSONObject params = new JSONObject();
    JSONObject params2 = new JSONObject();
    JSONObject params4 = new JSONObject();
    RequestParams param3= new RequestParams();
    private String token="";
    private String token3;
    private String result;
    private boolean feesIn=false;
    private RFCardReaderDevice device;
    private boolean checked=false;
    //protected ActionCallback mCallback;
    Card rfCard;
    List<String> taxes = new ArrayList<String>();
    List<String> payments = new ArrayList<String>();
    List<String> peage = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
      // TextView mode=findViewById(R.id.mode);
        EditText editText2=findViewById(R. id. textarea);
        EditText num=findViewById(R. id. editText3);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(home.this, android.R.layout.simple_spinner_item, taxes);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(home.this, android.R.layout.simple_spinner_item, payments);
        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(home.this, android.R.layout.simple_spinner_item, peage);
        ImageButton img=findViewById(R.id.button_backspace);
        Button one=findViewById(R.id.button_one);
        Button two=findViewById(R.id.button_two);
        Button three=findViewById(R.id.button_three);
        Button four=findViewById(R.id.button_four);
        Button five=findViewById(R.id.button_five);
        Button six=findViewById(R.id.button_six);
        Button seven=findViewById(R.id.button_seven);
        Button eight=findViewById(R.id.button_eight);
        Button nine=findViewById(R.id.button_nine);
        Button zero=findViewById(R.id.button_zero);
        Button doubleZero=findViewById(R.id.button_undred);
        Button tripleZero=findViewById(R.id.button_thousand);
        Button dot=findViewById(R.id.button_dot);
       // TextView txt=findViewById(R.id.deconnexion);
        ActionBar mActionBar = getSupportActionBar();
        //mActionBar.setDisplayShowHomeEnabled(false);
       // mActionBar.setDisplayShowTitleEnabled(false);

        LayoutInflater mInflater = LayoutInflater.from(this);
        Spinner spin = findViewById(R.id.spinner1);
        Spinner spin2 = findViewById(R.id.spinner);
        EditText spin3 = findViewById(R.id.spinner2);
        TextInputLayout tx=findViewById(R.id.lay);
        TextInputLayout tx2=findViewById(R.id.lay2);

        tx.setVisibility(View.GONE);
        tx2.setVisibility(View.GONE);
        TextView txtVw=findViewById(R.id.txtVw);
        spin.setVisibility(View.GONE);
        txtVw.setVisibility(View.GONE);
        View mCustomView = mInflater.inflate(R.layout.homeappbar, null);
        TextView txt = (TextView) mCustomView.findViewById(R.id.deconnexion2);

        ProgressBar loader = findViewById(R.id.progressBar);


        /*
        try {
            auth3();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        */
        payments.add("Paiement e-mpata");
        payments.add("Paiement taxes");
        payments.add("Paiement e-peage");
        taxes.add("Note de perception");
        taxes.add("Déclaration");
        taxes.add("Avis mise en recouvrement type 1");
        taxes.add("Avis mise en recouvrement type 2");
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin2.setAdapter(adapter2);
        Button button=findViewById(R. id. button);
        SharedPreferences sharedPref= PreferenceManager.getDefaultSharedPreferences(home.this);
        SharedPreferences.Editor editor2=sharedPref.edit();

        /*
        checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener()
        {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) //Line A
            {
                if(isChecked){
                    SharedPreferences.Editor editor=sharedPref.edit();
                    editor.putBoolean("isChecked", true);
                    editor.apply();
                    spin.setVisibility(View.VISIBLE);



                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spin.setAdapter(adapter);
                }else{
                    SharedPreferences.Editor editor=sharedPref.edit();
                    editor.putBoolean("isChecked", false);
                    editor.apply();
                    spin.setVisibility(View.GONE);

                }
            }
        });

         */
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spin.setAdapter(adapter);
        spin2.setOnItemSelectedListener(this);
        spin.setOnItemSelectedListener(this);
        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);
        new OnSwipeTouchListener(home.this) {
            @Override
            public void onSwipeRight() {
                Intent i3=new Intent(home.this,detailsTransaction.class);
                home.this.startActivity(i3);
                home.this.finish();
            }
        };

        editText2.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                editText2.setFocusable(false);
                return false;
            }
        });

        num.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                num.setFocusable(false);
                return false;
            }
        });
        int[] colors = new int[] {
                Color.WHITE,
                Color.WHITE,
        };

        int [][] states = new int [][]{
                new int[] { android.R.attr.state_enabled, -android.R.attr.state_checked},
                new int[] {android.R.attr.state_enabled, android.R.attr.state_checked}
        };

        BottomNavigationView bottomNavigation = findViewById(R.id.bottom_navigation);
        bottomNavigation.setOnNavigationItemSelectedListener( new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.navigation_home:
                        Intent i3=new Intent(home.this,home.class);
                        Bundle bndlanimation =
                                ActivityOptions.makeCustomAnimation(getApplicationContext(), R.transition.animation,R.transition.animation2).toBundle();
                        home.this.startActivity(i3);
                        home.this.finish();
                        return true;
                    case R.id.navigation_transactions:
                        Intent i=new Intent(home.this,detailsTransaction.class);
                        Bundle bndlanimation2 =
                                ActivityOptions.makeCustomAnimation(getApplicationContext(), R.transition.animation,R.transition.animation2).toBundle();
                        home.this.startActivity(i);
                        home.this.finish();
                        return true;

                }
                return false;
            }
        });


        ScrollView scroll=findViewById(R.id.scroll);
        scroll.setVerticalScrollBarEnabled(false);
        scroll.setHorizontalScrollBarEnabled(false);

        EditText editText=findViewById(R. id. editText3);



        SharedPreferences.Editor editor=sharedPref.edit();
        txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                editor.clear();
                editor.apply();
                Intent i = new Intent(home.this, Login.class);
                startActivity(i);
                finish();
            }
        });

        Button usd=findViewById(R.id.button_divide);
        Button cdf=findViewById(R.id.button_multiply);
        usd.setOnTouchListener(new View.OnTouchListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public boolean onTouch(View view,MotionEvent event) {
                SharedPreferences.Editor editor=sharedPref.edit();
                editor.putString("currency","USD");
                editor.apply();
                 if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)){
                    usd.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.teal_200));
                }
                usd.setBackgroundColor(Color.parseColor("#515de1"));
                usd.setTextColor(Color.WHITE);
                if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)) {
                    cdf.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.white));
                }
                cdf.setBackgroundColor(Color.WHITE);
                cdf.setTextColor(Color.parseColor("#515de1"));
                return false;
            }

        });

        cdf.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public void onClick(View view) {
                SharedPreferences.Editor editor=sharedPref.edit();
                editor.putString("currency","CDF");
                editor.apply();
                if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)){
                    cdf.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.teal_200));
                }

                cdf.setBackgroundColor(Color.parseColor("#515de1"));
                cdf.setTextColor(Color.WHITE);
                if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)) {
                    usd.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.white));
                }
                usd.setBackgroundColor(Color.WHITE);
                usd.setTextColor(Color.parseColor("#515de1"));
            }

        });


        //ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,R.array.planets_array,android.R.layout.);

        button.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                device = (RFCardReaderDevice) POSTerminal.getInstance(getApplicationContext())
                        .getDevice("cloudpos.device.rfcardreader");
            /*
                try {
                    Log.v("device mode", String.valueOf(device.getMode()));
                    mode.append(String.valueOf(device));
                    device.open();
                    Log.v("device mode", String.valueOf(device.getMode()));
                    mode.append(String.valueOf(device));
                } catch (DeviceException e) {
                    e.printStackTrace();
                }

                try {
                    sendSuccessLog("");
                    OperationResult operationResult = device.waitForCardPresent(TimeConstants.FOREVER);
                    if (operationResult.getResultCode() == OperationResult.SUCCESS) {
                        sendSuccessLog2(mContext.getString(R.string.find_card_succeed));
                        rfCard = ((RFCardReaderOperationResult) operationResult).getCard();
                    } else {
                        sendFailedLog2(mContext.getString(R.string.find_card_failed));
                    }
                } catch (DeviceException e) {
                    e.printStackTrace();
                    sendFailedLog(mContext.getString(R.string.operation_failed));
                }
                */

                Pop.on(home.this)
                        .with()
                        .title("Validation du code PIN")
                        .cancelable(false)
                        .layout(R.layout.custom_pop)
                        .when(new Pop.Yah() {
                            @Override
                            public void clicked(DialogInterface dialog, @Nullable View view) {
                                try {
                                    SharedPreferences sharedPref= PreferenceManager.getDefaultSharedPreferences(home.this);
                                    SharedPreferences.Editor editor=sharedPref.edit();
                                    EditText etName = (EditText) view.findViewById(R.id.pin);
                                    editor.putString("pin", String.valueOf(etName.getText()));
                                    editor.apply();
                                    ProgressBar loader= findViewById(R.id.progressBar);
                                    loader.setVisibility(View.VISIBLE);
                                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                    authenticate2();
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }

                            }
                        })
                        .show(new Pop.View() { // assign value to view element
                            @Override
                            public void prepare(View view) {
                                EditText etName = (EditText) view.findViewById(R.id.pin);
                                Log.i(TAG, "etName :: " + etName.getText());
                                etName.setText(etName.getText());
                            }
                        });

            }
        });
        img.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                editText2.setText("");
                return true;
            }
        });
        img.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                editText2.setText(clearCharacter(editText2.getText().toString()));
            }
        });

        one.setOnTouchListener(new View.OnTouchListener() {

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)){
                        one.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.teal_200));
                    }
                    one.setBackgroundColor(Color.parseColor("#515de1"));
                    one.setTextColor(Color.WHITE);

                }else if(event.getAction() == MotionEvent.ACTION_UP){
                    if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)){
                        one.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.white));
                    }
                    one.setBackgroundColor(Color.WHITE);
                    one.setTextColor(Color.parseColor("#515de1"));
                }

                return false;
            }

        });
        one.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                editText2.append("1");
            }
        });
        two.setOnTouchListener(new View.OnTouchListener() {

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)){
                        two.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.teal_200));
                    }
                    two.setBackgroundColor(Color.parseColor("#515de1"));
                    two.setTextColor(Color.WHITE);

                }else if(event.getAction() == MotionEvent.ACTION_UP){
                    if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)){
                        two.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.white));
                    }
                    two.setBackgroundColor(Color.WHITE);
                    two.setTextColor(Color.parseColor("#515de1"));
                }
                return false;
            }

        });
        two.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                editText2.append("2");
            }
        });
        three.setOnTouchListener(new View.OnTouchListener() {

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public boolean onTouch(View view, MotionEvent event) {


                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)){
                        three.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.teal_200));
                    }
                    three.setBackgroundColor(Color.parseColor("#515de1"));
                    three.setTextColor(Color.WHITE);

                }else if(event.getAction() == MotionEvent.ACTION_UP){
                    if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)){
                        three.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.white));
                    }
                    three.setBackgroundColor(Color.WHITE);
                    three.setTextColor(Color.parseColor("#515de1"));
                }
                return false;
            }

        });
        three.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                editText2.append("3");
            }
        });
        four.setOnTouchListener(new View.OnTouchListener() {

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)){
                       four.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.teal_200));
                    }
                    four.setBackgroundColor(Color.parseColor("#515de1"));
                    four.setTextColor(Color.WHITE);

                }else if(event.getAction() == MotionEvent.ACTION_UP){
                    if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)){
                        four.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.white));
                    }
                    four.setBackgroundColor(Color.WHITE);
                    four.setTextColor(Color.parseColor("#515de1"));
                }
                return false;
            }

        });
        four.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                editText2.append("4");
            }
        });
        five.setOnTouchListener(new View.OnTouchListener() {

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)){
                        five.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.teal_200));
                    }
                    five.setBackgroundColor(Color.parseColor("#515de1"));
                    five.setTextColor(Color.WHITE);

                }else if(event.getAction() == MotionEvent.ACTION_UP){
                    if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)){
                        five.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.white));
                    }
                    five.setBackgroundColor(Color.WHITE);
                    five.setTextColor(Color.parseColor("#515de1"));
                }
                return false;
            }

        });
        five.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                editText2.append("5");
            }
        });
        six.setOnTouchListener(new View.OnTouchListener() {

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)){
                        six.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.teal_200));
                    }
                    six.setBackgroundColor(Color.parseColor("#515de1"));
                    six.setTextColor(Color.WHITE);

                }else if(event.getAction() == MotionEvent.ACTION_UP){
                    if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)){
                        six.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.white));
                    }
                    six.setBackgroundColor(Color.WHITE);
                    six.setTextColor(Color.parseColor("#515de1"));
                }
                return false;
            }

        });
        six.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                editText2.append("6");
            }
        });
        seven.setOnTouchListener(new View.OnTouchListener() {

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)){
                        seven.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.teal_200));
                    }
                    seven.setBackgroundColor(Color.parseColor("#515de1"));
                    seven.setTextColor(Color.WHITE);

                }else if(event.getAction() == MotionEvent.ACTION_UP){
                    if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)){
                        seven.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.white));
                    }
                    seven.setBackgroundColor(Color.WHITE);
                    seven.setTextColor(Color.parseColor("#515de1"));
                }
                return false;
            }

        });
        seven.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                editText2.append("7");
            }
        });
        eight.setOnTouchListener(new View.OnTouchListener() {

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)){
                        eight.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.teal_200));
                    }
                    eight.setBackgroundColor(Color.parseColor("#515de1"));
                    eight.setTextColor(Color.WHITE);

                }else if(event.getAction() == MotionEvent.ACTION_UP){
                    if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)){
                        eight.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.white));
                    }
                    eight.setBackgroundColor(Color.WHITE);
                    eight.setTextColor(Color.parseColor("#515de1"));
                }
                return false;
            }

        });
        eight.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                editText2.append("8");
            }
        });
        nine.setOnTouchListener(new View.OnTouchListener() {

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)){
                        nine.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.teal_200));
                    }
                    nine.setBackgroundColor(Color.parseColor("#515de1"));
                    nine.setTextColor(Color.WHITE);

                }else if(event.getAction() == MotionEvent.ACTION_UP){
                    if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)){
                        nine.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.white));
                    }
                    nine.setBackgroundColor(Color.WHITE);
                    nine.setTextColor(Color.parseColor("#515de1"));
                }
                return false;
            }

        });
        nine.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                editText2.append("9");
            }
        });
        zero.setOnTouchListener(new View.OnTouchListener() {

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)){
                        zero.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.white));
                    }
                    zero.setBackgroundColor(Color.parseColor("#515de1"));
                    zero.setTextColor(Color.WHITE);

                }else if(event.getAction() == MotionEvent.ACTION_UP){
                    if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)){
                        zero.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.white));
                    }
                    zero.setBackgroundColor(Color.WHITE);
                    zero.setTextColor(Color.parseColor("#515de1"));
                }
                return false;
            }

        });
        zero.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                editText2.append("0");
            }
        });
        doubleZero.setOnTouchListener(new View.OnTouchListener() {

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)){
                        doubleZero.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.teal_200));
                    }
                    doubleZero.setBackgroundColor(Color.parseColor("#515de1"));
                    doubleZero.setTextColor(Color.WHITE);

                }else if(event.getAction() == MotionEvent.ACTION_UP){
                    if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)){
                        doubleZero.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.white));
                    }
                    doubleZero.setBackgroundColor(Color.WHITE);
                    doubleZero.setTextColor(Color.parseColor("#515de1"));
                }
                return false;
            }

        });
        doubleZero.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                editText2.append("00");
            }
        });
        tripleZero.setOnTouchListener(new View.OnTouchListener() {

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)){
                        tripleZero.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.teal_200));
                    }
                    tripleZero.setBackgroundColor(Color.parseColor("#515de1"));
                    tripleZero.setTextColor(Color.WHITE);

                }else if(event.getAction() == MotionEvent.ACTION_UP){
                    if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)){
                        tripleZero.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.white));
                    }
                    tripleZero.setBackgroundColor(Color.WHITE);
                    tripleZero.setTextColor(Color.parseColor("#515de1"));
                }
                return false;
            }

        });
        tripleZero.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                editText2.append("000");
            }
        });

        dot.setOnTouchListener(new View.OnTouchListener() {

            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
            @Override
            public boolean onTouch(View view, MotionEvent event) {

                if(event.getAction() == MotionEvent.ACTION_DOWN){
                    if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)){
                        dot.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.teal_200));
                    }
                    dot.setBackgroundColor(Color.parseColor("#515de1"));
                    dot.setTextColor(Color.WHITE);

                }else if(event.getAction() == MotionEvent.ACTION_UP){
                    if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)){
                        dot.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.white));
                    }
                    dot.setBackgroundColor(Color.WHITE);
                    dot.setTextColor(Color.parseColor("#515de1"));
                }


                return false;
            }

        });
        dot.setOnClickListener(new View.OnClickListener(){

            @Override
            public void onClick(View v) {
                editText2.append(".");
            }
        });

        //Initialise NfcAdapter

        nfcAdapter = NfcAdapter.getDefaultAdapter(this);

        //If no NfcAdapter, display that the device has no NFC
        /*
        if (nfcAdapter == null){
            Toast.makeText(this,"NO NFC Capabilities",
                    Toast.LENGTH_SHORT).show();
            finish();
        }

         */
        //Create a PendingIntent object so the Android system can
        //populate it with the details of the tag when it is scanned.
        //PendingIntent.getActivity(Context,requestcode(identifier for
        //                           intent),intent,int)
        pendingIntent = PendingIntent.getActivity(this,0,new Intent(this,this.getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP),0);

    }


    public String clearCharacter(String str) {
        if (str != null && str.length() > 0) {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }


    private String authenticate2() throws JSONException, UnsupportedEncodingException {
        params2.put("application","xXMXlK5dsy2mCHY9Li0Q");
        params2.put("password","password");
        params2.put("username","admin@wallet.com");
        StringEntity entity = new StringEntity(params2.toString());
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(home.this, "http://51.38.42.38:8080/ws/authenticate", entity,"application/json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response){
                try {
                    getUserAccountId(response.optString("jwttoken"));
                } catch (JSONException e) {
                    e.printStackTrace();
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }
                token=response.optString("jwttoken");
                Log.v("token",token);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                // token3=errorResponse.toString();
                //Log.v("token",token3);
            }


        });
        return token;
    }

    private String authenticate3() throws JSONException, UnsupportedEncodingException {
        params2.put("application","xXMXlK5dsy2mCHY9Li0Q");
        params2.put("password","password");
        params2.put("username","admin@wallet.com");
        StringEntity entity = new StringEntity(params2.toString());
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(home.this, "http://51.38.42.38:8080/ws/authenticate", entity,"application/json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response){
                validateBillNumber(response.optString("jwttoken"));
                token=response.optString("jwttoken");
                Log.v("token",token);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                // token3=errorResponse.toString();
                //Log.v("token",token3);
            }


        });
        return token;
    }

    private String authenticate4() throws JSONException, UnsupportedEncodingException {
        params2.put("application","xXMXlK5dsy2mCHY9Li0Q");
        params2.put("password","password");
        params2.put("username","admin@wallet.com");
        StringEntity entity = new StringEntity(params2.toString());
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(home.this, "http://51.38.42.38:8080/ws/authenticate", entity,"application/json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response){
                payTax(response.optString("jwttoken"));
                token=response.optString("jwttoken");
                Log.v("token",token);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                // token3=errorResponse.toString();
                //Log.v("token",token3);
            }


        });
        return token;
    }


    public String auth3() throws JSONException, UnsupportedEncodingException {
        Log.v("isCalled","true");
        params2.put("application","xXMXlK5dsy2mCHY9Li0Q");
        params2.put("password","password");
        params2.put("username","admin@wallet.com");
        StringEntity entity = new StringEntity(params2.toString());
        AsyncHttpClient client = new AsyncHttpClient();
        client.post(home.this, "http://51.38.42.38:8080/ws/authenticate", entity,"application/json", new JsonHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, JSONObject response){
                getTaxes(response.optString("jwttoken"));
                token=response.optString("jwttoken");
                Log.v("token",token);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                // token3=errorResponse.toString();
                //Log.v("token",token3);
            }


        });
        return token;
    }
        private void getTaxes(String token){
        Log.v("isCalled2","true");
            List<Header> headers = new ArrayList<Header>();
            headers.add(new BasicHeader("Accept", "application/json"));
            //headers.add(new BasicHeader("Content-Type", "application/json"));
            Log.v("token2", token);
            headers.add(new BasicHeader("Authorization", "Bearer " + token));

            TransactionClient.get3(home.this, "tax/document/all", headers.toArray(new Header[headers.size()]),param3,
                    new JsonHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers2, JSONArray response) {
                    Spinner spin = (Spinner) findViewById(R.id.spinner1);
                    try {
                    for (int i = 0; i < response.length(); i++) {

                            JSONObject object = response.getJSONObject(i);
                            if(taxes.size()!=4){
                                taxes.add(object.getString("libelle"));
                            }
                    Log.v("taxe2",taxes.get(i));

                    }

                     } catch (JSONException e) {
                            e.printStackTrace();
                        }


                }

                @Override
                public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                    Log.v("message",errorResponse.optString("message"));
                }
            });
}

private void payPeage(String token,int accountId,int merchant){
        Log.v("isMe","true");
    List<Header> headers = new ArrayList<Header>();
    headers.add(new BasicHeader("Accept", "application/json"));
    headers.add(new BasicHeader("Content-Type", "application/json"));
    //headers.add(new BasicHeader("Content-Type", "application/json"));
    Log.v("token2", token);
    headers.add(new BasicHeader("Authorization", "Bearer " + token));
    ProgressBar loader = findViewById(R.id.progressBar);
    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(home.this);
    Date date = new Date();
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String dat = format.format(date);
    String[] str = dat.split(" ");
    dat = str[0] + 'T' + str[1];
    EditText bill=findViewById(R.id.spinner2);
    try {
        EditText edit=findViewById(R.id.textarea);
        EditText card=findViewById(R.id.editText3);
        Log.v("sender", String.valueOf(card.getText()));
        Log.v("amount", String.valueOf(edit.getText()));
        Log.v("billNumber", String.valueOf(bill.getText()));
        Log.v("currency", sharedPref.getString("currency", "CDF"));
        Log.v("merchant", sharedPref.getString("accountId", "1"));
        Log.v("paymentDate", dat);
        //SharedPreferences.Editor editor = sharedPref.edit();
        //editor.putInt("merchant", Integer.parseInt(sharedPref.getString("accountId", "1")));
        //editor.apply();
        params4.put("sender", card.getText());
        params4.put("billNumber", bill.getText());
        params4.put("amount", edit.getText());
        params4.put("currency", sharedPref.getString("currency", "CDF"));
        params4.put("feesIn", true);
        params4.put("merchant", merchant);
        params4.put("paymentDate", dat);
        StringEntity entity2 = new StringEntity(params4.toString());
        entity2.setContentEncoding(new BasicHeader("Content-Type", "application/json"));
        entity2.setContentType(new BasicHeader("Content-Type", "application/json"));
        Log.v("content", String.valueOf(entity2.getContent()));
        Log.v("params", params4.toString());
        Log.v("params2", entity2.toString());
        //params2.put("amount", amount.getText());
        TransactionClient.get(home.this, "peage/pay-bill", headers.toArray(new Header[headers.size()]), entity2, "application/json",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers2, JSONObject response2) {
                        loader.setVisibility(View.INVISIBLE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        Button btn=findViewById(R.id.button);
                        btn.setEnabled(false);
                        Log.v("isMe2","true");
                        EditText area=findViewById(R.id.textarea);
                        EditText spin=findViewById(R.id.spinner2);
                        EditText edt=findViewById(R.id.editText3);
                        area.setText("");
                        spin.setText("");
                        edt.setText("");
                        try {
                            TransactionClient.get2(home.this, "account/history/" + response2.getJSONObject("account").optString("accountId"), headers.toArray(new Header[headers.size()]), param3,
                                    new JsonHttpResponseHandler() {

                                        @Override
                                        public void onSuccess(int statusCode, Header[] headers, JSONObject  response) {

                                        }

                                        @Override
                                        public void onSuccess(int statusCode, Header[] headers,  JSONArray  response) {

                                            List<Transaction> txs = null;
                                            int a = 0;
                                            Intent i4 = new Intent(home.this, detailsTransaction.class);
                                            try {

                                                Transaction transaction = new Transaction();
                                                Gson gson = new Gson();
                                                Intent i3 = new Intent(home.this, detailsTransaction.class);


                                                for (int i = 0; i < response.length(); i++) {
                                                    JSONObject object = response.getJSONObject(i);
                                                    Object operationType = object.get("operationType");
                                                    Object othe = object.get("other");
                                                    Object acc = object.get("account");

                                                    if (operationType instanceof JSONObject && othe instanceof JSONObject && acc instanceof JSONObject) {
                                                        if (object.getJSONObject("operationType").getInt("operationTypeId") == 21 ||
                                                                object.getJSONObject("operationType").getInt("operationTypeId") == 23 ||
                                                                object.getJSONObject("operationType").getInt("operationTypeId") == 8) {
                                                            a++;
                                                            JSONObject account = object.getJSONObject("account");
                                                            JSONObject other = object.getJSONObject("other");
                                                            int accountId = account.getInt("accountId");
                                                            JSONObject user = account.getJSONObject("user");
                                                            JSONObject user2 = other.getJSONObject("user");
                                                            String lastName = user.getString("lastName");
                                                            int operationId = object.getInt("operationId");
                                                            Log.v("operationId", String.valueOf(object.getInt("operationId")));
                                                            transaction.setReference(object.getString("reference"));
                                                            transaction.setOperationId(object.getInt("operationId"));
                                                            transaction.setDate(object.getString("operationDate"));
                                                            transaction.setUserId1(user.getInt("userId"));
                                                            transaction.setUserId2(user2.getInt("userId"));
                                                            transaction.setEmailBen(user2.getString("email"));
                                                            transaction.setEmailEm(user.getString("email"));
                                                            transaction.setNomEm(user.getString("lastName"));
                                                            transaction.setNomBen(user2.getString("lastName"));
                                                            transaction.setPrenomBen(user2.getString("firstName"));
                                                            transaction.setPrenomEm(user.getString("email"));
                                                            transaction.setNumCard(object.getString("card"));
                                                            transaction.setType(object.getJSONObject("operationType").getString("libelle"));
                                                            transaction.setMontant(object.getInt("amount"));
                                                            transaction.setCurrency(object.getJSONObject("devise").getString("code"));
                                                            transaction.setEmetteur(String.valueOf(account.getInt("msisdn")));
                                                            transaction.setMarchant(other.getInt("msisdn"));
                                                            String transJson = gson.toJson(transaction);
                                                            loader.setVisibility(View.INVISIBLE);
                                                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                                            // Toast.makeText(getApplicationContext(), "Login success !", Toast.LENGTH_SHORT).show();
                                                            editor.putString("transactions" + String.valueOf(i), transJson);
                                                            //Log.v("transactions:", sharedPref.getString("transactions"+i,"trans"));
                                                            editor.apply();
                                                        }
                                                    }
                                                }

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            editor.putInt("transNumber", a);
                                            editor.apply();
                                            Log.v("transRam", String.valueOf(sharedPref.getInt("transNumber",1)));
                                            editor.putString("accountId", response2.optString("accountId"));
                                            editor.apply();
                                            editor.putBoolean("isLoggedIn", true);
                                            editor.apply();
                                            loader.setVisibility(View.INVISIBLE);
                                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                            // Toast.makeText(getApplicationContext(), "Login success !", Toast.LENGTH_LONG).show();
                                            //Intent i3 = new Intent(home.this, detailsTransaction.class);
                                            // home.this.startActivity(i3);
                                            //home.this.finish();
                                        }

                                        @Override
                                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                                            Toast.makeText(getApplicationContext(), errorResponse.optString("message"), Toast.LENGTH_LONG).show();
                                        }

                                        @Override
                                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                                            try {
                                                Toast.makeText(getApplicationContext(), errorResponse.getJSONObject(3).toString(), Toast.LENGTH_LONG).show();
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }

                                        @Override
                                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                            //super.onFailure(statusCode, headers, responseString, throwable);
                                            Log.v("error", responseString);
                                        }

                                        @Override
                                        public void onSuccess(int statusCode, Header[] headers, String responseString) {
                                            // super.onSuccess(statusCode, headers, responseString);
                                            Log.v("result", responseString);
                                        }

                                    });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(getApplicationContext(), "Transaction effectuée avec succès !", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        loader.setVisibility(View.INVISIBLE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        EditText area=findViewById(R.id.textarea);
                        EditText spin=findViewById(R.id.spinner2);
                        EditText edt=findViewById(R.id.editText3);
                        area.setText("");
                        spin.setText("");
                        edt.setText("");
                        Toast.makeText(getApplicationContext(), errorResponse.optString("message"), Toast.LENGTH_LONG).show();
                    }
                });
    } catch (JSONException | UnsupportedEncodingException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    }
}

    private void validateBillNumber(String token){
        List<Header> headers = new ArrayList<Header>();
        headers.add(new BasicHeader("Accept", "application/json"));
        //headers.add(new BasicHeader("Content-Type", "application/json"));
        Log.v("token2", token);
        headers.add(new BasicHeader("Authorization", "Bearer " + token));
        ProgressBar loader = findViewById(R.id.progressBar);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(home.this);
        EditText bill=findViewById(R.id.spinner2);
        param3.put("billNumber",bill.getText());
        Log.v("path","peage/check-bill/"+bill.getText());
        TransactionClient.get3(home.this, "peage/check-bill/"+bill.getText(), headers.toArray(new Header[headers.size()]),param3,
                new JsonHttpResponseHandler() {
                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject  response) {
                        Button button=findViewById(R. id. button);
                        button.setEnabled(true);
                        SharedPreferences.Editor editor=sharedPref.edit();
                        editor.putString("amount",response.optString("amount"));
                        editor.putString("currency",response.optString("currency"));
                        editor.apply();
                        EditText montant=findViewById(R.id.textarea);
                        montant.setText(response.optString("amount"));
                        montant.setEnabled(false);
                        Button usd=findViewById(R.id.button_divide);
                        Button cdf=findViewById(R.id.button_multiply);
                       if(response.optString("currency").equals("USD")) {
                           if (Build.VERSION_CODES.LOLLIPOP <= Integer.valueOf(android.os.Build.VERSION.SDK)) {
                               usd.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.teal_200));
                           }
                           usd.setBackgroundColor(Color.parseColor("#515de1"));
                           usd.setTextColor(Color.WHITE);
                           if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)) {
                               cdf.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.white));
                           }
                           cdf.setBackgroundColor(Color.WHITE);
                           cdf.setTextColor(Color.parseColor("#515de1"));

                       }else if(response.optString("currency").equals("CDF")){
                           if (Build.VERSION_CODES.LOLLIPOP <= Integer.valueOf(android.os.Build.VERSION.SDK)) {
                               cdf.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.teal_200));
                           }
                           cdf.setBackgroundColor(Color.parseColor("#515de1"));
                           cdf.setTextColor(Color.WHITE);
                           if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)) {
                               usd.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.white));
                           }
                           usd.setBackgroundColor(Color.WHITE);
                           usd.setTextColor(Color.parseColor("#515de1"));

                       }
                        loader.setVisibility(View.INVISIBLE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        Toast.makeText(getApplicationContext(), "ticket valide", Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        loader.setVisibility(View.INVISIBLE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        Log.v("me",errorResponse.optString("message"));
                       // Toast.makeText(getApplicationContext(), res, Toast.LENGTH_LONG).show();
                    }


                });
    }

private void payTax(String token){
    List<Header> headers = new ArrayList<Header>();
    headers.add(new BasicHeader("Accept", "application/json"));
    //headers.add(new BasicHeader("Content-Type", "application/json"));
    Log.v("token2", token);
    headers.add(new BasicHeader("Authorization", "Bearer " + token));
    ProgressBar loader = findViewById(R.id.progressBar);
    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(home.this);
    Date date = new Date();
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String dat = format.format(date);
    String[] str = dat.split(" ");
    dat = str[0] + 'T' + str[1];
    try {
        EditText ed=findViewById(R.id.editTextTextPersonName);
        EditText edit=findViewById(R.id.textarea);


        //params2.put("account", accountId);
        //params2.put("amount", edit.getText());
        //params2.put("currency", sharedPref.getString("currency", "CDF"));
        param3.put("document", sharedPref.getLong("taxe", 1));
        param3.put("documentNumber", ed.getText());
        //params2.put("paymentDate", dat);
        StringEntity entity2 = new StringEntity(params2.toString());
        Log.v("url","tax/document/verify/"+sharedPref.getLong("taxe", 1)+"/"+ed.getText());
        TransactionClient.get2(home.this, "tax/document/verify/"+sharedPref.getLong("taxe", 1)+"/"+ed.getText(),headers.toArray(new Header[headers.size()]),param3,
                new JsonHttpResponseHandler() {

                    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                    @Override
                    public void onSuccess(int statusCode, Header[] headers2, JSONObject response) {
                        Log.v("result","true");
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString("currency",response.optString("currency"));
                        editor.putString("amount",response.optString("amount"));
                        editor.apply();
                        EditText montant=findViewById(R.id.textarea);
                        montant.setText(response.optString("amount"));
                        montant.setEnabled(false);
                        Button btn=findViewById(R.id.button);
                        btn.setEnabled(true);
                        Button usd=findViewById(R.id.button_divide);
                        Button cdf=findViewById(R.id.button_multiply);
                        if(response.optString("currency").equals("USD")) {
                            if (Build.VERSION_CODES.LOLLIPOP <= Integer.valueOf(android.os.Build.VERSION.SDK)) {
                                usd.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.teal_200));
                            }
                            usd.setBackgroundColor(Color.parseColor("#515de1"));
                            usd.setTextColor(Color.WHITE);
                            if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)) {
                                cdf.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.white));
                            }
                            cdf.setBackgroundColor(Color.WHITE);
                            cdf.setTextColor(Color.parseColor("#515de1"));

                        }else if(response.optString("currency").equals("CDF")){
                            if (Build.VERSION_CODES.LOLLIPOP <= Integer.valueOf(android.os.Build.VERSION.SDK)) {
                                cdf.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.teal_200));
                            }
                            cdf.setBackgroundColor(Color.parseColor("#515de1"));
                            cdf.setTextColor(Color.WHITE);
                            if(Build.VERSION_CODES.LOLLIPOP<=Integer.valueOf(android.os.Build.VERSION.SDK)) {
                                usd.setBackgroundTintList(ContextCompat.getColorStateList(getApplicationContext(), R.color.white));
                            }
                            usd.setBackgroundColor(Color.WHITE);
                            usd.setTextColor(Color.parseColor("#515de1"));

                        }
                        loader.setVisibility(View.INVISIBLE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        Toast.makeText(getApplicationContext(), response.optString("message"), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        loader.setVisibility(View.INVISIBLE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        Log.v("statuscode", String.valueOf(statusCode));
                        Toast.makeText(getApplicationContext(), errorResponse.optString("message"), Toast.LENGTH_SHORT).show();
                    }
                });
    } catch (UnsupportedEncodingException e) {
        e.printStackTrace();
    }
}

private void validateTax(String token,int merchant){
    Log.v("isMe","true");
    List<Header> headers = new ArrayList<Header>();
    headers.add(new BasicHeader("Accept", "application/json"));
    headers.add(new BasicHeader("Content-Type", "application/json"));
    //headers.add(new BasicHeader("Content-Type", "application/json"));
    Log.v("token2", token);
    headers.add(new BasicHeader("Authorization", "Bearer " + token));
    ProgressBar loader = findViewById(R.id.progressBar);
    SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(home.this);
    Date date = new Date();
    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    String dat = format.format(date);
    String[] str = dat.split(" ");
    dat = str[0] + 'T' + str[1];
    EditText bill=findViewById(R.id.editTextTextPersonName);
    try {
        EditText edit=findViewById(R.id.textarea);
        EditText card=findViewById(R.id.editText3);
        Log.v("sender", String.valueOf(card.getText()));
        Log.v("amount", sharedPref.getString("amount", "1"));
        Log.v("documentNumber", String.valueOf(bill.getText()));
        Log.v("currency", sharedPref.getString("currency", "CDF"));
        Log.v("merchant", String.valueOf(merchant));
        Log.v("paymentDate", dat);
        //SharedPreferences.Editor editor = sharedPref.edit();
        //editor.putInt("merchant", Integer.parseInt(sharedPref.getString("accountId", "1")));
        //editor.apply();
        params4.put("sender", card.getText());
        params4.put("documentNumber", bill.getText());
        params4.put("document",sharedPref.getLong("taxe",1));
        params4.put("amount", sharedPref.getString("amount", "1"));
        params4.put("currency", sharedPref.getString("currency", "CDF"));
        params4.put("feesIn", true);
        params4.put("merchant", merchant);
        params4.put("paymentDate", dat);
        StringEntity entity2 = new StringEntity(params4.toString());
        entity2.setContentEncoding(new BasicHeader("Content-Type", "application/json"));
        entity2.setContentType(new BasicHeader("Content-Type", "application/json"));
        Log.v("content", String.valueOf(entity2.getContent()));
        Log.v("params", params4.toString());
        Log.v("params2", entity2.toString());
        //params2.put("amount", amount.getText());
        TransactionClient.get(home.this, "tax/pay", headers.toArray(new Header[headers.size()]), entity2, "application/json",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers2, JSONObject response2) {
                        loader.setVisibility(View.INVISIBLE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        Button btn=findViewById(R.id.button);
                        btn.setEnabled(false);
                        Log.v("isMe2","true");
                        EditText area=findViewById(R.id.textarea);
                        EditText spin=findViewById(R.id.spinner2);
                        EditText edt=findViewById(R.id.editText3);
                        area.setText("");
                        spin.setText("");
                        edt.setText("");
                        try {
                            TransactionClient.get2(home.this, "account/history/" + response2.getJSONObject("account").optString("accountId"), headers.toArray(new Header[headers.size()]), param3,
                                    new JsonHttpResponseHandler() {

                                        @Override
                                        public void onSuccess(int statusCode, Header[] headers, JSONObject  response) {

                                        }

                                        @Override
                                        public void onSuccess(int statusCode, Header[] headers,  JSONArray  response) {

                                            List<Transaction> txs = null;
                                            int a = 0;
                                            Intent i4 = new Intent(home.this, detailsTransaction.class);
                                            try {

                                                Transaction transaction = new Transaction();
                                                Gson gson = new Gson();
                                                Intent i3 = new Intent(home.this, detailsTransaction.class);


                                                for (int i = 0; i < response.length(); i++) {
                                                    JSONObject object = response.getJSONObject(i);
                                                    Object operationType = object.get("operationType");
                                                    Object othe = object.get("other");
                                                    Object acc = object.get("account");

                                                    if (operationType instanceof JSONObject && othe instanceof JSONObject && acc instanceof JSONObject) {
                                                        if (object.getJSONObject("operationType").getInt("operationTypeId") == 21 ||
                                                                object.getJSONObject("operationType").getInt("operationTypeId") == 23 ||
                                                                object.getJSONObject("operationType").getInt("operationTypeId") == 8) {
                                                            a++;
                                                            JSONObject account = object.getJSONObject("account");
                                                            JSONObject other = object.getJSONObject("other");
                                                            int accountId = account.getInt("accountId");
                                                            JSONObject user = account.getJSONObject("user");
                                                            JSONObject user2 = other.getJSONObject("user");
                                                            String lastName = user.getString("lastName");
                                                            int operationId = object.getInt("operationId");
                                                            Log.v("operationId", String.valueOf(object.getInt("operationId")));
                                                            transaction.setReference(object.getString("reference"));
                                                            transaction.setOperationId(object.getInt("operationId"));
                                                            transaction.setDate(object.getString("operationDate"));
                                                            transaction.setUserId1(user.getInt("userId"));
                                                            transaction.setUserId2(user2.getInt("userId"));
                                                            transaction.setEmailBen(user2.getString("email"));
                                                            transaction.setEmailEm(user.getString("email"));
                                                            transaction.setNomEm(user.getString("lastName"));
                                                            transaction.setNomBen(user2.getString("lastName"));
                                                            transaction.setPrenomBen(user2.getString("firstName"));
                                                            transaction.setPrenomEm(user.getString("email"));
                                                            transaction.setNumCard(object.getString("card"));
                                                            transaction.setType(object.getJSONObject("operationType").getString("libelle"));
                                                            transaction.setMontant(object.getInt("amount"));
                                                            transaction.setCurrency(object.getJSONObject("devise").getString("code"));
                                                            transaction.setEmetteur(String.valueOf(account.getInt("msisdn")));
                                                            transaction.setMarchant(other.getInt("msisdn"));
                                                            String transJson = gson.toJson(transaction);
                                                            loader.setVisibility(View.INVISIBLE);
                                                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                                            // Toast.makeText(getApplicationContext(), "Login success !", Toast.LENGTH_SHORT).show();
                                                            editor.putString("transactions" + String.valueOf(i), transJson);
                                                            //Log.v("transactions:", sharedPref.getString("transactions"+i,"trans"));
                                                            editor.apply();
                                                        }
                                                    }
                                                }

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            editor.putInt("transNumber", a);
                                            editor.apply();
                                            Log.v("transRam", String.valueOf(sharedPref.getInt("transNumber",1)));
                                            editor.putString("accountId", response2.optString("accountId"));
                                            editor.apply();
                                            editor.putBoolean("isLoggedIn", true);
                                            editor.apply();
                                            loader.setVisibility(View.INVISIBLE);
                                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                            // Toast.makeText(getApplicationContext(), "Login success !", Toast.LENGTH_LONG).show();
                                            //Intent i3 = new Intent(home.this, detailsTransaction.class);
                                            // home.this.startActivity(i3);
                                            //home.this.finish();
                                        }

                                        @Override
                                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                                            Toast.makeText(getApplicationContext(), errorResponse.optString("message"), Toast.LENGTH_LONG).show();
                                        }

                                        @Override
                                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                                            try {
                                                Toast.makeText(getApplicationContext(), errorResponse.getJSONObject(3).toString(), Toast.LENGTH_LONG).show();
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }

                                        @Override
                                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                            //super.onFailure(statusCode, headers, responseString, throwable);
                                            Log.v("error", responseString);
                                        }

                                        @Override
                                        public void onSuccess(int statusCode, Header[] headers, String responseString) {
                                            // super.onSuccess(statusCode, headers, responseString);
                                            Log.v("result", responseString);
                                        }

                                    });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(getApplicationContext(), "Transaction effectuée avec succès !", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        loader.setVisibility(View.INVISIBLE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        EditText area=findViewById(R.id.textarea);
                        EditText spin=findViewById(R.id.spinner2);
                        EditText edt=findViewById(R.id.editText3);
                        area.setText("");
                        spin.setText("");
                        edt.setText("");
                        Toast.makeText(getApplicationContext(), errorResponse.optString("message"), Toast.LENGTH_LONG).show();
                    }
                });
    } catch (JSONException | UnsupportedEncodingException e) {
        e.printStackTrace();
    } catch (IOException e) {
        e.printStackTrace();
    }

}



    private void getTransanctions(String token,int accountId) throws JSONException, UnsupportedEncodingException {
        List<Header> headers = new ArrayList<Header>();
        headers.add(new BasicHeader("Accept", "application/json"));
        //headers.add(new BasicHeader("Content-Type", "application/json"));
        Log.v("token2", token);
        headers.add(new BasicHeader("Authorization", "Bearer " + token));
        EditText editText = findViewById(R.id.editText3);
        EditText editText2 = findViewById(R.id.textarea);
        EditText editText3 = findViewById(R.id.editText2);
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(home.this);
        feesIn = sharedPref.getBoolean("feesIn", false);
        Date date = new Date();
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String dat = format.format(date);
        String[] str = dat.split(" ");
        dat = str[0] + 'T' + str[1];
        Log.v("date", dat);
        Log.v("accountId", sharedPref.getString("accountId", "1"));
        if (editText2.getText().equals("") || editText.getText().equals("")) {
            Toast.makeText(getApplicationContext(), "Veuillez remplir tous les champs", Toast.LENGTH_SHORT).show();
        }else{
        params.put("amount", editText2.getText());
        params.put("currency", sharedPref.getString("currency", "CDF"));
        params.put("date", dat);
        params.put("description", "Paiement NFC");
        params.put("feesIn", false);
        params.put("merchant", sharedPref.getString("accountId", "1"));
        params.put("operationType", 21);
        params.put("sender", editText.getText());
        StringEntity entity = new StringEntity(params.toString());
            String finalDat = dat;
            TransactionClient.get(home.this, "operation/nfc", headers.toArray(new Header[headers.size()]), entity, "application/json",
                new JsonHttpResponseHandler() {
                    ProgressBar loader = findViewById(R.id.progressBar);
                    @Override
                    public void onSuccess(int statusCode, Header[] headers2, JSONObject response2) {
                        if(!sharedPref.getBoolean("isChecked",false)) {
                            loader.setVisibility(View.INVISIBLE);
                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        }
                        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(home.this);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        EditText editText2 = findViewById(R.id.textarea);
                        editText2.setText("");
                        EditText editText3 = findViewById(R.id.editText3);
                        editText3.setText("");
                        try {
                            Log.v("url","account/history/" + response2.getJSONObject("account").optString("accountId"));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }



                        Log.v("history",response2.optString("accountId"));
                        try {
                            TransactionClient.get2(home.this, "account/history/" + response2.getJSONObject("account").optString("accountId"), headers.toArray(new Header[headers.size()]), param3,
                                    new JsonHttpResponseHandler() {

                                        @Override
                                        public void onSuccess(int statusCode, Header[] headers, JSONObject  response) {

                                        }

                                        @Override
                                        public void onSuccess(int statusCode, Header[] headers,  JSONArray  response) {

                                            List<Transaction> txs = null;
                                            int a = 0;
                                            Intent i4 = new Intent(home.this, detailsTransaction.class);
                                            try {

                                                Transaction transaction = new Transaction();
                                                Gson gson = new Gson();
                                                Intent i3 = new Intent(home.this, detailsTransaction.class);


                                                for (int i = 0; i < response.length(); i++) {
                                                    JSONObject object = response.getJSONObject(i);
                                                    Object operationType = object.get("operationType");
                                                    Object othe = object.get("other");
                                                    Object acc = object.get("account");

                                                    if (operationType instanceof JSONObject && othe instanceof JSONObject && acc instanceof JSONObject) {
                                                        if (object.getJSONObject("operationType").getInt("operationTypeId") == 21 ||
                                                                object.getJSONObject("operationType").getInt("operationTypeId") == 23 ||
                                                                object.getJSONObject("operationType").getInt("operationTypeId") == 8) {
                                                            a++;
                                                            JSONObject account = object.getJSONObject("account");
                                                            JSONObject other = object.getJSONObject("other");
                                                            int accountId = account.getInt("accountId");
                                                            JSONObject user = account.getJSONObject("user");
                                                            JSONObject user2 = other.getJSONObject("user");
                                                            String lastName = user.getString("lastName");
                                                            int operationId = object.getInt("operationId");
                                                            Log.v("operationId", String.valueOf(object.getInt("operationId")));
                                                            transaction.setReference(object.getString("reference"));
                                                            transaction.setOperationId(object.getInt("operationId"));
                                                            transaction.setDate(object.getString("operationDate"));
                                                            transaction.setUserId1(user.getInt("userId"));
                                                            transaction.setUserId2(user2.getInt("userId"));
                                                            transaction.setEmailBen(user2.getString("email"));
                                                            transaction.setEmailEm(user.getString("email"));
                                                            transaction.setNomEm(user.getString("lastName"));
                                                            transaction.setNomBen(user2.getString("lastName"));
                                                            transaction.setPrenomBen(user2.getString("firstName"));
                                                            transaction.setPrenomEm(user.getString("email"));
                                                            transaction.setNumCard(object.getString("card"));
                                                            transaction.setType(object.getJSONObject("operationType").getString("libelle"));
                                                            transaction.setMontant(object.getInt("amount"));
                                                            transaction.setCurrency(object.getJSONObject("devise").getString("code"));
                                                            transaction.setEmetteur(String.valueOf(account.getInt("msisdn")));
                                                            transaction.setMarchant(other.getInt("msisdn"));
                                                            String transJson = gson.toJson(transaction);
                                                            loader.setVisibility(View.INVISIBLE);
                                                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                                            // Toast.makeText(getApplicationContext(), "Login success !", Toast.LENGTH_SHORT).show();
                                                            editor.putString("transactions" + String.valueOf(i), transJson);
                                                            //Log.v("transactions:", sharedPref.getString("transactions"+i,"trans"));
                                                            editor.apply();
                                                        }
                                                    }
                                                }

                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                            editor.putInt("transNumber", a);
                                            editor.apply();
                                            Log.v("transRam", String.valueOf(sharedPref.getInt("transNumber",1)));
                                            editor.putString("accountId", response2.optString("accountId"));
                                            editor.apply();
                                            editor.putBoolean("isLoggedIn", true);
                                            editor.apply();
                                            loader.setVisibility(View.INVISIBLE);
                                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                           // Toast.makeText(getApplicationContext(), "Login success !", Toast.LENGTH_LONG).show();
                                            //Intent i3 = new Intent(home.this, detailsTransaction.class);
                                           // home.this.startActivity(i3);
                                            //home.this.finish();
                                        }

                                        @Override
                                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                                            Toast.makeText(getApplicationContext(), errorResponse.optString("message"), Toast.LENGTH_LONG).show();
                                        }

                                        @Override
                                        public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                                            try {
                                                Toast.makeText(getApplicationContext(), errorResponse.getJSONObject(3).toString(), Toast.LENGTH_LONG).show();
                                            } catch (JSONException e) {
                                                e.printStackTrace();
                                            }
                                        }

                                        @Override
                                        public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                                            //super.onFailure(statusCode, headers, responseString, throwable);
                                          Log.v("error", responseString);
                                        }

                                        @Override
                                        public void onSuccess(int statusCode, Header[] headers, String responseString) {
                                           // super.onSuccess(statusCode, headers, responseString);
                                            Log.v("result", responseString);
                                        }

                                    });
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        Toast.makeText(getApplicationContext(), "Transaction effectuée avec succès !", Toast.LENGTH_SHORT).show();
                        //  Intent i3=new Intent(home.this,TransactionDetails.class);
                        // home.this.startActivity(i3);
                        //home.this.finish();
                    }



                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        //Toast.makeText(getApplicationContext(), "Transaction effectuée avec succès !", Toast.LENGTH_SHORT).show();
                        Log.v("history",responseString);
                    }



                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {

                        if(!sharedPref.getBoolean("isChecked",false)) {
                            loader.setVisibility(View.INVISIBLE);
                            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        }
                        result = String.valueOf(errorResponse);
                        Log.v("result", result);
                        try {
                            Toast.makeText(getApplicationContext(), errorResponse.getString("message"), Toast.LENGTH_SHORT).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }


                });
    }
    }



    private void getUserAccountId(String token) throws JSONException, UnsupportedEncodingException{
        List<Header> headers = new ArrayList<Header>();
        headers.add(new BasicHeader("Accept", "application/json"));
        headers.add(new BasicHeader("Authorization", "Bearer "+token));
        SharedPreferences sharedPref= PreferenceManager.getDefaultSharedPreferences(home.this);
        EditText editText = findViewById(R.id.editText3);
        Log.v("numcard", String.valueOf(editText.getText()));
        param3.put("number",editText.getText());
        TransactionClient.get2(home.this, "account/nfc/getBy/"+editText.getText(),headers.toArray(new Header[headers.size()]),param3,
                new JsonHttpResponseHandler() {
                    ProgressBar loader= findViewById(R.id.progressBar);
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {

                        try {
                            //Toast.makeText(getApplicationContext(), response.getString("error"), Toast.LENGTH_LONG).show();
                            // Toast.makeText(getApplicationContext(), response.getString("status"), Toast.LENGTH_LONG).show();
                            if (statusCode==200) {
                                //SharedPreferences.Editor editor = sharedPref.edit();
                               // editor.putInt("accountId2", response.getInt("accountId"));
                               // editor.apply();
                                Log.v("userAccountId", String.valueOf(response.getInt("accountId")));
                                validatePIN(token,response.getInt("accountId"));
                            } else {
                                loader.setVisibility(View.INVISIBLE);
                                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                Toast.makeText(getApplicationContext(), response.getString("message"), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        loader.setVisibility(View.INVISIBLE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        try {
                            Toast.makeText(getApplicationContext(),response.getJSONObject(0).getString("message"),Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        Toast.makeText(getApplicationContext(),responseString,Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void onProgress(long bytesWritten, long totalSize) {

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        loader.setVisibility(View.INVISIBLE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        result=responseString;
                        Log.v("result",result);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        loader.setVisibility(View.INVISIBLE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        Toast.makeText(getApplicationContext(),errorResponse.optString("message"),Toast.LENGTH_LONG).show();
                        result=String.valueOf(errorResponse);
                        Log.v("result",result);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        loader.setVisibility(View.INVISIBLE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        result=String.valueOf(errorResponse);
                        Log.v("result",result);
                    }
                });
    }

    private void validatePIN(String token,int accountId) throws JSONException, UnsupportedEncodingException {
        List<Header> headers = new ArrayList<Header>();
        headers.add(new BasicHeader("Accept", "application/json"));
        SharedPreferences sharedPref= PreferenceManager.getDefaultSharedPreferences(home.this);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("merchant", Integer.parseInt(sharedPref.getString("accountId", "1")));
        editor.apply();
        int merchant=sharedPref.getInt("merchant",1);
        param3.put("account_id",accountId);
        EditText etName = findViewById(R.id.pin);
        EditText editText2=findViewById(R. id. textarea);
        param3.put("pin",sharedPref.getString("pin","1"));
        //headers.add(new BasicHeader("Content-Type", "application/json"));
        Log.v("token2",token);
        headers.add(new BasicHeader("Authorization", "Bearer "+token));


        StringEntity entity = new StringEntity(params.toString());
        Log.v("path:","operation/pin/"+sharedPref.getString("pin","1")+"/"+accountId);
        TransactionClient.get2(home.this, "operation/pin/"+sharedPref.getString("pin","1")+"/"+accountId,headers.toArray(new Header[headers.size()]),param3,
                new JsonHttpResponseHandler() {
                    ProgressBar loader= findViewById(R.id.progressBar);
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        Log.v("taxe", String.valueOf(sharedPref.getBoolean("taxeForm",false)));                        Log.v("response2", String.valueOf(sharedPref.getBoolean("taxeForm",false)));
                        Log.v("empata", String.valueOf(sharedPref.getBoolean("empataForm",false)));
                        Log.v("epeage", String.valueOf(sharedPref.getBoolean("epeageForm",false)));

                        try {
                            //Toast.makeText(getApplicationContext(), response.getString("error"), Toast.LENGTH_LONG).show();
                           // Toast.makeText(getApplicationContext(), response.getString("status"), Toast.LENGTH_LONG).show();
                            if (response.getString("status").equals("OK")) {
                                if(sharedPref.getBoolean("taxeForm",false)==true) {
                                    validateTax(token,merchant);
                                }else if(sharedPref.getBoolean("empataForm",false)==true){
                                    getTransanctions(token,accountId);
                                }else if(sharedPref.getBoolean("epeageForm",false)==true){
                                    payPeage(token,accountId,merchant);
                                }
                            } else {
                                loader.setVisibility(View.INVISIBLE);
                                getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                Toast.makeText(getApplicationContext(), response.getString("error"), Toast.LENGTH_LONG).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (UnsupportedEncodingException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        loader.setVisibility(View.INVISIBLE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        try {
                            Toast.makeText(getApplicationContext(),response.getJSONObject(0).getString("error"),Toast.LENGTH_LONG).show();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, String responseString) {
                        Toast.makeText(getApplicationContext(),responseString,Toast.LENGTH_LONG).show();

                    }

                    @Override
                    public void onProgress(long bytesWritten, long totalSize) {

                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        loader.setVisibility(View.INVISIBLE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        result=responseString;
                        Log.v("result",result);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        loader.setVisibility(View.INVISIBLE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        Toast.makeText(getApplicationContext(),errorResponse.optString("error"),Toast.LENGTH_LONG).show();
                        result=String.valueOf(errorResponse);
                        Log.v("result",result);
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONArray errorResponse) {
                        loader.setVisibility(View.INVISIBLE);
                        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                        result=String.valueOf(errorResponse);
                        Log.v("result",result);
                    }
                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        //assert nfcAdapter != null;
        //nfcAdapter.enableForegroundDispatch(context,pendingIntent,
        //                                    intentFilterArray,
        //                                    techListsArray)
        if(nfcAdapter!=null){
        nfcAdapter.enableForegroundDispatch(this,pendingIntent,null,null);
        }else{
            Toast.makeText(this,"Veuillez activer votre nfc",
                    Toast.LENGTH_SHORT).show();
        }
    }
/*
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    private void readNdef(Intent intent) {

        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(intent.getAction())) {

            final Parcelable[] rawMessages =
                    intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            Log.v("message", String.valueOf(rawMessages));
            if (rawMessages != null) {
                final NdefMessage[] messages = new NdefMessage[rawMessages.length];
                // NDEF data is encapsulated inside an NdefMessage that contains one or more
                // NdefRecord
                for (int i = 0; i < rawMessages.length; i++) {
                    messages[i] = (NdefMessage) rawMessages[i];
                    for (NdefRecord ndefRecord : messages[i].getRecords()) {
                        final String message =
                                new String(ndefRecord.getPayload(), StandardCharsets.UTF_8);
                        Toast.makeText(home.this, "Tag: " + message, Toast.LENGTH_SHORT)
                                .show();
                        Log.v("message",message);
                    }
                }
            }
        }
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())) {

           // final Parcelable[] rawMessages =
              //      intent.getParcelableArrayExtra(NfcAdapter.EXTRA_TAG);
            Log.v("message", NfcAdapter.EXTRA_DATA);

        }
    }


 */
    protected void onPause() {
        super.onPause();
        //Onpause stop listening
        if (nfcAdapter != null) {
            nfcAdapter.disableForegroundDispatch(this);
        }
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        setIntent(intent);
        resolveIntent(intent);

    }

    private void resolveIntent(Intent intent) {
        String action = intent.getAction();
        Log.v("action",action);
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            Tag tag = (Tag) intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            byte[] payload = detectTagData(tag).getBytes();
        }
    }
    public String getRandomString(){
        char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
        StringBuilder sb = new StringBuilder(4);
        Random random = new Random();
        for (int i = 0; i < 4; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        String output = sb.toString();
        Log.v("new string",output);
        return output;
    }
    public String readTag(MifareUltralight mifareUlTag) {
        try {
            mifareUlTag.connect();
            byte[] payload = mifareUlTag.readPages(5);
            String output=new String(payload, Charset.forName("US-ASCII"));
            Log.v("data2", new String(payload, Charset.forName("US-ASCII")));
            //TextView vi=findViewById(R.id.textView4);
            //vi.setText(output.substring(0,4));
            //Toast.makeText(this,"ramses",Toast.LENGTH_SHORT).show();
            //Toast.makeText(home.this,output.substring(0,4),Toast.LENGTH_SHORT).show();
            return new String(payload, Charset.forName("US-ASCII"));
        } catch (IOException e) {
            Log.e(TAG, "IOException while reading MifareUltralight message...", e);
        } finally {
            if (mifareUlTag != null) {
                try {
                    mifareUlTag.close();
                }
                catch (IOException e) {
                    Log.e(TAG, "Error closing tag...", e);
                }
            }
        }
        return null;
    }
    public void writeTag(MifareUltralight mifareUlTag) {
        try {
            mifareUlTag.connect();
            String ramses=getRandomString();

           // mifareUlTag.writePage(4, "ramses".getBytes(Charset.forName("US-ASCII")));
           // mifareUlTag.writePage(5, "kouam".getBytes(Charset.forName("US-ASCII")));
            mifareUlTag.writePage(5, ramses.getBytes(Charset.forName("US-ASCII")));
            //mifareUlTag.writePage(7, " now".getBytes(Charset.forName("US-ASCII")));
        } catch (IOException e) {
            Log.e(TAG, "IOException while writing MifareUltralight...", e);
        } finally {
            try {
                mifareUlTag.close();
            } catch (IOException e) {
                Log.e(TAG, "IOException while closing MifareUltralight...", e);
            }
        }
    }

    private void write(Tag tag) {
        Log.v("ramses","ramses");
        Ndef ndef = Ndef.get(tag);
        Locale locale = Locale.ENGLISH;
        NdefRecord hi = createTextRecord("ramses", locale, true);
        NdefMessage mNdefMessage = new NdefMessage(hi);

        try{
            ndef.connect();
            ndef.writeNdefMessage(mNdefMessage);
            Toast.makeText(this, "Message Written", Toast.LENGTH_LONG).show();
        }
        catch (Exception e){
            Log.d(TAG, "Exception:  " + e.toString());
        }
        finally {
            try{
                ndef.close();
            }
            catch(Exception e){
                Log.d(TAG, ":( no  " + e.toString());
            }
        }
    }

    public NdefRecord createTextRecord(String payload, Locale locale, boolean encodeInUtf8) {
        byte[] langBytes = locale.getLanguage().getBytes(Charset.forName("US-ASCII"));
        Charset utfEncoding = encodeInUtf8 ? Charset.forName("UTF-8") : Charset.forName("UTF-16");
        byte[] textBytes = payload.getBytes(utfEncoding);
        int utfBit = encodeInUtf8 ? 0 : (1 << 7);
        char status = (char) (utfBit + langBytes.length);
        byte[] data = new byte[1 + langBytes.length + textBytes.length];
        data[0] = (byte) status;
        System.arraycopy(langBytes, 0, data, 1, langBytes.length);
        System.arraycopy(textBytes, 0, data, 1 + langBytes.length, textBytes.length);
        NdefRecord record = new NdefRecord(NdefRecord.TNF_WELL_KNOWN,
                NdefRecord.RTD_TEXT, new byte[0], data);
        return record;
    }

    private void read(Tag tagFromIntent) {
        Ndef ndef = Ndef.get(tagFromIntent);

        try {
            ndef.connect();
            NdefMessage mNdefMessage = ndef.getNdefMessage();
            NdefRecord[] records = mNdefMessage.getRecords();
            byte[] payload = records[0].getPayload();
            String displayString = getTextFromNdefRecord(records[0]);
            Log.v("data",displayString);
            //displayInfo.setText(displayString);
            Toast.makeText(this, "String read", Toast.LENGTH_LONG).show();
        } catch (Exception e) {
            Log.d(TAG, e.toString());
        } finally {
            try {
                ndef.close();
            } catch (Exception e) {
                Log.d(TAG, e.toString());
            }
        }
    }

        public String getTextFromNdefRecord(NdefRecord ndefRecord)
        {
            String tagContent = null;
            try {
                byte[] payload = ndefRecord.getPayload();
                String textEncoding = "UTF-8";
                int languageSize = payload[0] & 0063;
                tagContent = new String(payload, languageSize + 1,
                        payload.length - languageSize - 1, textEncoding);
            } catch (UnsupportedEncodingException e) {
                Log.e("getTextFromNdefRecord", e.getMessage(), e);
            }
            return tagContent;
        }

    private String detectTagData(Tag tag) {
        StringBuilder sb = new StringBuilder();
        byte[] id = tag.getId();
        Log.v("tag",new String(tag.getId()));
        EditText editText=findViewById(R. id. editText3);
        editText.setText(toHex(id));
        editText.setEnabled(false);
        sb.append("ID (hex): ").append(toHex(id)).append('\n');
        sb.append("ID (reversed hex): ").append(toReversedHex(id)).append('\n');
        sb.append("ID (dec): ").append(toDec(id)).append('\n');
        sb.append("ID (reversed dec): ").append(toReversedDec(id)).append('\n');

        String prefix = "android.nfc.tech.";
        sb.append("Technologies: ");
        for (String tech : tag.getTechList()) {
            sb.append(tech.substring(prefix.length()));
            sb.append(", ");

            if (tech.equals(MifareUltralight.class.getName())) {
                MifareUltralight mifareUlTag = MifareUltralight.get(tag);
                //writeTag(mifareUlTag);
                readTag(mifareUlTag);
            }
        }

        sb.delete(sb.length() - 2, sb.length());

        for (String tech : tag.getTechList()) {
            if (tech.equals(MifareClassic.class.getName())) {
                sb.append('\n');
                String type = "Unknown";

                try {
                    MifareClassic mifareTag = MifareClassic.get(tag);

                    switch (mifareTag.getType()) {
                        case MifareClassic.TYPE_CLASSIC:
                            type = "Classic";
                            break;
                        case MifareClassic.TYPE_PLUS:
                            type = "Plus";
                            break;
                        case MifareClassic.TYPE_PRO:
                            type = "Pro";
                            break;
                    }
                    sb.append("Mifare Classic type: ");
                    sb.append(type);
                    sb.append('\n');

                    sb.append("Mifare size: ");
                    sb.append(mifareTag.getSize() + " bytes");
                    sb.append('\n');

                    sb.append("Mifare sectors: ");
                    sb.append(mifareTag.getSectorCount());
                    sb.append('\n');

                    sb.append("Mifare blocks: ");
                    sb.append(mifareTag.getBlockCount());
                } catch (Exception e) {
                    sb.append("Mifare classic error: " + e.getMessage());
                }
            }

            if (tech.equals(MifareUltralight.class.getName())) {
                sb.append('\n');
                MifareUltralight mifareUlTag = MifareUltralight.get(tag);
                String type = "Unknown";
                switch (mifareUlTag.getType()) {
                    case MifareUltralight.TYPE_ULTRALIGHT:
                        type = "Ultralight";
                        break;
                    case MifareUltralight.TYPE_ULTRALIGHT_C:
                        type = "Ultralight C";
                        break;
                }
                sb.append("Mifare Ultralight type: ");
                sb.append(type);
            }
        }
        Log.v("test",sb.toString());
        return sb.toString();
    }
    private String toHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = bytes.length - 1; i >= 0; --i) {
            int b = bytes[i] & 0xff;
            if (b < 0x10)
                sb.append('0');
            sb.append(Integer.toHexString(b));
            if (i > 0) {
                sb.append(" ");
            }
        }
        return sb.toString();
    }

    private String toReversedHex(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; ++i) {
            if (i > 0) {
                sb.append(" ");
            }
            int b = bytes[i] & 0xff;
            if (b < 0x10)
                sb.append('0');
            sb.append(Integer.toHexString(b));
        }
        return sb.toString();
    }

    private long toDec(byte[] bytes) {
        long result = 0;
        long factor = 1;
        for (int i = 0; i < bytes.length; ++i) {
            long value = bytes[i] & 0xffl;
            result += value * factor;
            factor *= 256l;
        }
        return result;
    }

    private long toReversedDec(byte[] bytes) {
        long result = 0;
        long factor = 1;
        for (int i = bytes.length - 1; i >= 0; --i) {
            long value = bytes[i] & 0xffl;
            result += value * factor;
            factor *= 256l;
        }
        return result;
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Log.v("pay", String.valueOf(parent.getId()));
        SharedPreferences sharedPref = PreferenceManager.getDefaultSharedPreferences(home.this);
        SharedPreferences.Editor editor=sharedPref.edit();
        switch(parent.getId()){
            case R.id.spinner1:
                editor.putLong("taxe", parent.getItemIdAtPosition(position)+1);
                editor.apply();
                break;
            case R.id.spinner:
                Spinner spin = findViewById(R.id.spinner1);
                TextView txtVw=findViewById(R.id.txtVw);
                EditText spin2=findViewById(R.id.spinner2);
                TextInputLayout numdoc=findViewById(R.id.lay2);
                Button button=findViewById(R. id. button);
                EditText area=findViewById(R.id.textarea);
                EditText doc=findViewById(R.id.editTextTextPersonName);
                ProgressBar loader = findViewById(R.id.progressBar);
                TextInputLayout txt=findViewById(R.id.lay);

                if(parent.getItemIdAtPosition(position)==0){


                    button.setEnabled(true);
                    txtVw.setVisibility(View.GONE);
                    spin.setVisibility(View.GONE);
                    spin2.setVisibility(View.GONE);
                    txt.setVisibility(View.GONE);
                    numdoc.setVisibility(View.GONE);
                    editor.putBoolean("empataForm",true);
                    editor.putBoolean("taxeForm",false);
                    editor.putBoolean("epeageForm",false);
                    editor.apply();

                }else if(parent.getItemIdAtPosition(position)==1){
                    button.setEnabled(false);
                    txtVw.setVisibility(View.VISIBLE);
                    spin.setVisibility(View.VISIBLE);
                    spin2.setVisibility(View.GONE);
                    txt.setVisibility(View.GONE);
                    numdoc.setVisibility(View.VISIBLE);
                    doc.setOnKeyListener(new View.OnKeyListener()
                    {

                        @Override
                        public boolean onKey(View v, int keyCode, KeyEvent event)
                        {
                            Log.v("keycode", String.valueOf(KeyEvent.KEYCODE_ENTER));
                            Log.v("keycode", String.valueOf(keyCode));
                            Log.v("event", String.valueOf(event.getAction()));
                            Log.v("event", String.valueOf(KeyEvent.ACTION_DOWN));

                            if((keyCode==KeyEvent.KEYCODE_ENTER) && (event.getAction() == KeyEvent.ACTION_DOWN))
                            {
                                try {
                                    loader.setVisibility(View.VISIBLE);
                                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                    authenticate4();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                                return false;

                            }

                            return false;
                        }
                    });

                    editor.putBoolean("empataForm",false);
                    editor.putBoolean("taxeForm",true);
                    editor.putBoolean("epeageForm",false);
                    editor.apply();
                    area.setFocusable(false);
                }else if(parent.getItemIdAtPosition(position)==2){


                    area.setFocusable(false);
                    button.setEnabled(false);
                    txtVw.setVisibility(View.GONE);
                    spin.setVisibility(View.GONE);
                    spin2.setVisibility(View.VISIBLE);
                    txt.setVisibility(View.VISIBLE);
                    numdoc.setVisibility(View.GONE);
                    editor.putBoolean("empataForm",false);
                    editor.putBoolean("taxeForm",false);
                    editor.putBoolean("epeageForm",true);
                    editor.apply();

                    spin2.setOnKeyListener(new View.OnKeyListener()
                    {

                        @Override
                        public boolean onKey(View v, int keyCode, KeyEvent event)
                        {
                            Log.v("keycode", String.valueOf(KeyEvent.KEYCODE_ENTER));
                            Log.v("keycode", String.valueOf(keyCode));
                            Log.v("event", String.valueOf(event.getAction()));
                            Log.v("event", String.valueOf(KeyEvent.ACTION_DOWN));

                            if((keyCode==KeyEvent.KEYCODE_ENTER) && (event.getAction() == KeyEvent.ACTION_DOWN))
                            {
                                try {
                                    loader.setVisibility(View.VISIBLE);
                                    getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
                                    authenticate3();

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                } catch (UnsupportedEncodingException e) {
                                    e.printStackTrace();
                                }
                                return false;

                            }

                            return false;
                        }
                    });

                }
                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }
}