package com.example.empatanfc;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        Handler handler=new Handler();
        handler.postDelayed(new Runnable(){
            public void run(){
                Intent i3=new Intent(MainActivity.this,Login.class);
                MainActivity.this.startActivity(i3);
                MainActivity.this.finish();
            }
        },2000);

    }


}