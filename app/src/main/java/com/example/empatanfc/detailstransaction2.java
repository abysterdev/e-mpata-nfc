package com.example.empatanfc;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.example.e_mpatanfc.models.models.Transaction;
import com.google.gson.Gson;

public class detailstransaction2 extends Activity  {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detailstransaction2);
        Intent intent=getIntent();
        String transaction;
        transaction=intent.getStringExtra("transItem");
        Gson gson = new Gson();
        Transaction trans=gson.fromJson(transaction,Transaction.class);
        //ImageView img=findViewById(R.id.imageView5);
        TextView transId=findViewById(R.id.transIDval);
        TextView ref=findViewById(R.id.Reférenceval);
        TextView montant=findViewById(R.id.Montantval);
        TextView type=findViewById(R.id.Typeval);
        TextView date=findViewById(R.id.Dateval);
        TextView userId1=findViewById(R.id.UserID1val);
        TextView userId2=findViewById(R.id.UserID2val);
        TextView nom1=findViewById(R.id.nameval);
        TextView nom2=findViewById(R.id.name2val);
        TextView email1=findViewById(R.id.emailval);
        TextView email2=findViewById(R.id.email2val);
        TextView phone1=findViewById(R.id.phoneval);
        TextView phone2=findViewById(R.id.phone2val);
        TextView carte=findViewById(R.id.carteval);
        transId.setText(String.valueOf(trans.getOperationId()));
        ref.setText(trans.getReference());
        montant.setText(trans.getMontant()+" "+trans.getCurrency());
        type.setText((trans.getType()));
        date.setText(trans.getDate());
        carte.setText(trans.getNumCard());
        if(String.valueOf(trans.getMontant()).startsWith("-")){
       userId1.setText(String.valueOf(trans.getUserId1()));
       userId2.setText(String.valueOf(trans.getUserId2()));
        nom1.setText(trans.getNomEm()+" "+trans.getPrenomEm());
        nom2.setText(trans.getNomBen()+" "+trans.getPrenomBen());
        email1.setText(trans.getEmailEm());
        email2.setText(trans.getEmailBen());
        phone1.setText(trans.getEmetteur());
        phone2.setText(String.valueOf(trans.getMarchant()));
        }else{
            userId1.setText(String.valueOf(trans.getUserId2()));
            userId2.setText(String.valueOf(trans.getUserId1()));
            nom1.setText(trans.getNomBen()+" "+trans.getPrenomBen());
            nom2.setText(trans.getNomEm()+" "+trans.getPrenomEm());
            email1.setText(trans.getEmailBen());
            email2.setText(trans.getEmailEm());
            phone1.setText(String.valueOf(trans.getMarchant()));
            phone2.setText(String.valueOf(trans.getEmetteur()));
        }
        /*
        img.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                Intent i3=new Intent(detailstransaction2.this,home.class);
                detailstransaction2.this.startActivity(i3);
                detailstransaction2.this.finish();
            }
        });

         */
        ScrollView scroll=findViewById(R.id.scroll3);
        scroll.setVerticalScrollBarEnabled(false);
        scroll.setHorizontalScrollBarEnabled(false);
    }


}