package com.example.e_mpatanfc.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.e_mpatanfc.models.models.Transaction;
import com.example.empatanfc.R;
import java.util.ArrayList;

public class Operation extends ArrayAdapter<Transaction> {
    private static class ViewHolder {
        TextView OperationId;
        TextView Reference;
        TextView Montant;
        TextView Beneficiaire;
        TextView Emetteur;
        TextView Status;
        TextView type;
        TextView date;
        TextView heure;
        TextView phone;
        TextView phone2;
        TextView userId1;
        TextView userId2;
    }
    public Operation(Context context, ArrayList<Transaction> transactions) {
        super(context,0, transactions);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        Transaction t = getItem(position);
        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.common_list, parent, false);
            viewHolder.Reference =  (TextView)convertView.findViewById(R.id.reference);
            viewHolder.date =  (TextView)convertView.findViewById(R.id.date);
            viewHolder.heure =  (TextView)convertView.findViewById(R.id.heure);
            viewHolder.Montant = (TextView) convertView.findViewById(R.id.montant);
            viewHolder.type = (TextView) convertView.findViewById(R.id.type);
            viewHolder.phone = (TextView) convertView.findViewById(R.id.phoneval);
            viewHolder.phone2 = (TextView) convertView.findViewById(R.id.phone2val);
            viewHolder.userId1 = (TextView) convertView.findViewById(R.id.UserID1val);
            viewHolder.userId2 = (TextView) convertView.findViewById(R.id.UserID2val);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.Reference.setText("Reférence: "+t.getReference());
        viewHolder.Montant.setText(String.valueOf(t.getMontant())+" "+t.getCurrency());
        viewHolder.type.setText(t.getType());
        viewHolder.heure.setText(t.getDate2());
        viewHolder.date.setText(t.getDate3());
        return convertView;
    }



}
