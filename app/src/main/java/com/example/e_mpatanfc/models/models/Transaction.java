package com.example.e_mpatanfc.models.models;
import android.util.Log;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Transaction implements Serializable {

    private int operationId;
    private String Reference;
    private String status;
    private int montant;
    private int marchant;
    private String emetteur;
    private String type;
    private String date;
    private int userId1;
    private int userId2;
    private String nomEm;
    private String prenomEm;
    private  String prenomBen;
    private String nomBen;
    private String emailEm;
    private String emailBen;
    private String numCard;
    private String currency;


    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getPrenomEm() {
        return prenomEm;
    }

    public void setPrenomEm(String prenomEm) {
        this.prenomEm = prenomEm;
    }

    public String getPrenomBen() {
        return prenomBen;
    }

    public void setPrenomBen(String prenomBen) {
        this.prenomBen = prenomBen;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDate() {
        String y = this.date.substring(0, 4);
        String m = this.date.substring(5, 7);
        String d = this.date.substring(8, 10);
        String t = this.date.substring(11, 19);
        return d+"/"+m+"/"+y+" "+t;
    }

    public String getDate2(){
       return this.date.substring(11, 19);
    }

    public String getDate3(){
        String y = this.date.substring(0, 4);
        String d = this.date.substring(8, 10);
        String month="";
        List<Map<String, String>> hmap = new ArrayList<Map<String, String>>();
        Map<String, String> map1 = Map.of("num","01", "letter","Janvier");
        Map<String, String> map2 = Map.of("num","02", "letter","Février");
        Map<String, String> map3 = Map.of("num","03", "letter","Mars");
        Map<String, String> map4 = Map.of("num","04", "letter","Avril");
        Map<String, String> map5 = Map.of("num","05", "letter","Mai");
        Map<String, String> map6 = Map.of("num","06", "letter","Juin");
        Map<String, String> map7 = Map.of("num","07", "letter","Juillet");
        Map<String, String> map8 = Map.of("num","08", "letter","Aout");
        Map<String, String> map9 = Map.of("num","09", "letter","Septembre");
        Map<String, String> map10 = Map.of("num","10", "letter","Octobre");
        Map<String, String> map11 = Map.of("num","11", "letter","Novembre");
        Map<String, String> map12 = Map.of("num","12", "letter","Décembre");

        hmap.add(map1);
        hmap.add(map2);
        hmap.add(map3);
        hmap.add(map4);
        hmap.add(map5);
        hmap.add(map6);
        hmap.add(map7);
        hmap.add(map8);
        hmap.add(map9);
        hmap.add(map10);
        hmap.add(map11);
        hmap.add(map12);

        for (Map m : hmap) {
            String m1 = this.date.substring(5, 7);
            Log.v("m1:", this.date.substring(5, 7));
            if (m1.equals(m.get("num"))) {
                month = (String) m.get("letter");
                Log.v("month:",(String) m.get("letter"));
            }
        }

        return d+" "+month+" "+y;

    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getUserId1() {
        return userId1;
    }

    public void setUserId1(int userId1) {
        this.userId1 = userId1;
    }

    public int getUserId2() {
        return userId2;
    }

    public void setUserId2(int userId2) {
        this.userId2 = userId2;
    }

    public String getNomEm() {
        return nomEm;
    }

    public void setNomEm(String nomEm) {
        this.nomEm = nomEm;
    }

    public String getNomBen() {
        return nomBen;
    }

    public void setNomBen(String nomBen) {
        this.nomBen = nomBen;
    }

    public String getEmailEm() {
        return emailEm;
    }

    public void setEmailEm(String emailEm) {
        this.emailEm = emailEm;
    }

    public String getEmailBen() {
        return emailBen;
    }

    public void setEmailBen(String emailBen) {
        this.emailBen = emailBen;
    }

    public String getNumCard() {
        return numCard;
    }

    public void setNumCard(String numCard) {
        this.numCard = numCard;
    }

    public int getOperationId() {
        return operationId;
    }

    public void setOperationId(int operationId) {
        this.operationId = operationId;
    }

    public String getReference() {
        return Reference;
    }

    public void setReference(String reference) {
        Reference = reference;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getMontant() {
        return montant;
    }

    public void setMontant(int montant) {
        this.montant = montant;
    }

    public int getMarchant() {
        return marchant;
    }

    public void setMarchant(int marchant) {
        this.marchant = marchant;
    }

    public String getEmetteur() {
        return emetteur;
    }

    public void setEmetteur(String emetteur) {
        this.emetteur = emetteur;
    }
/*
    public Transaction(JSONObject object) {
        try {
            this.operationId = object.getInt("operationId");
            this.Reference = object.getString("Reference");
            this.status = object.getString("status");
            this.montant = object.getInt("montant");
            this.marchant = object.getInt("marchant");
            this.emetteur=object.getString("emetteur");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
*/
    public Transaction() {
    }

    public Transaction(int operationId, String Reference, String status, int montant, int marchant, String emetteur, String type,
                       String date, int userId1, int userId2, String nomEm, String nomBen, String emailEm, String emailBen, String numCard,String prenomBen,String prenomEm,String currency) {
        this.operationId = operationId;
        this.Reference = Reference;
        this.emetteur=emetteur;
        this.marchant=marchant;
        this.montant=montant;
        this.status=status;
        this.type=type;
        this.date=date;
        this.userId1=userId1;
        this.userId2=userId2;
        this.nomBen=nomBen;
        this.nomEm=nomEm;
        this.emailBen=emailBen;
        this.emailEm=emailEm;
        this.numCard=numCard;
        this.prenomBen=prenomBen;
        this.prenomEm=prenomEm;
        this.currency=currency;
    }

}
